<?php
include("../private/DBConnect.php");
session_start();
if(isset($_SESSION['valid']) && $_SESSION['valid'] == true) {
	header("Location: /");
}
if($_POST) {
	$errMsg = "";
	include("../private/hCaptcha_info.php");

	$verify = curl_init();
	curl_setopt($verify, CURLOPT_URL, "https://hcaptcha.com/siteverify");
	curl_setopt($verify, CURLOPT_POST, true);
	curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
	curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($verify);
	$responseData = json_decode($response);
	if($responseData->success) {
		if(user_exists($_POST['username_new'])) {
			$errMsg .= "Username taken. ";
		}
	   if(empty($_POST['title'])) {
	   	$errMsg .= "Title cannot be blank. ";
	   }
	   if(empty($_POST['description'])) {
	   	$errMsg .= "Description cannot be blank. ";
	   }
	   if(empty($_POST['username_new'])) {
	   	$errMsg .= "Username cannot be blank. ";
	   }
	   if(empty($_POST['password_new'])) {
	   	$errMsg .= "Password cannot be blank. ";
	   }
	   if($_POST['password_new'] != $_POST['password_confirm']) {
	   	$errMsg .= "Passwords did not match. ";
	   }
	   if(preg_match("/^([^A-z][^0-9])$/", $_POST['username_new'])) {
	   	$errMsg .= "<br>Username can only contain the standard American alphabet and/or letters 0 through 9. No spaces allowed.<br>
	   	<small>For nerds, the regex is /^([^A-z][^0-9])$/</small>";
	   }
	   if(empty($errMsg)) {
	   	if(site_create($_POST['username_new'], $_POST['title'], $_POST['description'], password_hash($_POST['password_new']), PASSWORD_DEFAULT)) {
	   		$_SESSION['username'] == $_POST['username_new'];
	   		$_SESSION['valid'] == true;
	   		header('Location: /');
	   	} else {
	   		$errMsg = "A database error occured. Try again in a few minutes."; 
	   	}
	   } else {
	   	$errMsg = $errMsg;
	   }
	} else {
	   $errMsg = 'Robot verification failed, please try again.';
	}
}
?>
<html>
	<head>
        <link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=DotGothic16&family=Ubuntu&display=swap" rel="stylesheet"> 
		<style>* {font-family: Ubuntu; color: #eee;} html, body {background: #111;} input[type="text"], input[type="password"], input[type="submit"], select, textarea {background: white; color: black; border: none; border-radius: 5px; padding: 10px; margin: 10px; display: block;} .error {display: block; margin: 10px; font-size: 1em; color: red;}</style>
		<script src="https://hcaptcha.com/1/api.js" async defer></script>
		</head>
	<body>
		<center>
			<h1>Register</h1>
			<form method='post'>
				<b>What is the title of the site you're creating? (this does not have to be unique)</b><br>
				<input type='text' name='title' value="<?php echo $_POST['title'];?>"><br>

				<b>What will it be about?</b><br>
				<textarea placeholder="if your site isn't unlisted this will be displayed publically, so you should refer to yourself in the third person" name='description'>"<?php echo $_POST['description'];?>"</textarea><br><br>

				<b>Select the street you want your site to be on:</b><br>
				<small>(In lamens terms, what interests will be most prominent your site?)</small><br>
				<select name='street'>
					<option value='1'>Fortune Street:		Video games, puzzles, toys</option>
					<option value='2'>Paintbrush Street:	Drawings, photos, visual art</option>
					<option value='3'>Broadway:				Film, cartoons</option>
					<option value='4'>Sesame Street:		Information and education</option>
					<option value='5'>Animal Forest:		Animals, mythical or real</option>
					<option value='6'>Silicon Valley:		Hardware, software, programming</option>
					<option value='7'>Musical Road:			Music, things with melodies</option>
					<option value='8'>Ink Road:				Creative writing</option>
					<option value='9'>Tokyo Square:			Specifically Japanese anime</option>
					<option selected="selected" value='0'>No street - my site will be listed nowhere and I will share links to it myself.</option>
				</select><br><br>

				<b>Your username (this does have to be unique)</b>
				<input type='text' name='username_new' value="<?php echo $_POST['username_new'];?>" placeholder='Your site name'>
				<b>Your password</b>
				<input type='password' name='password_new' placeholder='Your password'>
				<b>Confirm said password</b>
				<input type='password' name='password_confirm' placeholder='Confirm your password'>

				<div class="h-captcha" data-sitekey="b6949f32-75b3-4f7c-a244-6f5459bfec49"></div>
				<input type='submit' value='Register'>
				<div class='error'><?php echo $errMsg;?></div>
			</form>
		</center>
	</body>
</html>