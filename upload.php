<?php 
include("../private/DBConnect.php");
session_start();
if(empty($_SESSION['USERNAME'])) {
	die("You must log in to create an account.<br>If you are seeing this and have logged in, make sure you have cookies enabled.")
}
if(password_verify($_POST['password'], $password)) {
	$_SESSION['valid'] = true;
	$_SESSION['username'] = $_POST['username'];
	header("Location: /");
} else {
	die("Incorrect password.");
}
$fp = fopen("../../private/sites/".$_SESSION['username']."/".$_SESSION['username']);
fwrite($fp, $_POST['contents']);
fclose($fp);
?>