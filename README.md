# hindsite
[![Discord](https://img.shields.io/discord/591914197219016707.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/qqqzdtv5W7)

**hindsite** is a service which aims to combine the best of Carrd and Neocities. It's simple to use (no coding knowledge needed) and emphasizes personal homepages like Carrd does, but you'll get the full amount of customization that Neocities gives you. It is currently in pre-alpha.
