<?php
// Display all errors.
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// PHP Includes.
include("res/Common.php");
include("res/ReadSite.php");

include("../private/DBConnect.php");
// Get the path name
$pathname = parse_url($_SERVER['REQUEST_URI'])['path']; $pathname = str_replace("/","",$pathname);
if(empty($pathname)) {$pathname = "home";}
?>

<!DOCTYPE html>
<html>
<head>
<title>hindsite</title>
<style>.box .box {position: relative!important;} a, a:visited {color: inherit; text-decoration-style: dotted;}</style>
</head>
<body background='<?php echo sites_get($pathname, "bgimage");?>'>

<?php

// Is the pathname blank? If so, read the home page.
if(ReadSite($pathname) == "404") {
    echo("<center><h3>This site doesn't exist. Make sure you typed it right or that you were given the correct link.");
}
?>

</body>
</html>
