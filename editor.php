<html>
    <head>
        <link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=DotGothic16&family=Ubuntu&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="res/Editor.css">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="res/CUSTOMIZED_jquery-ui.min.js"></script>
        <script src="res/GlobalVariables.js"></script>
        <script src="res/ContextOptions.js"></script> 
        <script src="res/ContextMenu.js"></script>
        <script src="res/ContextFunctions.js"></script>
        <script src="res/Editor.js"></script>
        <title>editor</title>
    </style>
    </head>
    <body id="doc">
    <span id="settings"></span>
    <span id="guide1"></span>
    <span id="guide2"></span>
    <span id="guide3"></span>
    <span id="guide4"></span>
    <span id="guide5"></span>
    <span id="canvas">
    <?php session_start();
    // PHP Includes.
    include("res/Common.php");
    include("res/ReadSite.php");
    include("../private/DBConnect.php");

    if($_GET['test'] == 1){
        ?>
        <span id="mouse_coords"></span>
    <?php
    }
    if(isset($_SESSION['username'])) {
        ReadSite($_SESSION['username'], "true");
    }
    ?>
    </span>
    </body>
</html>