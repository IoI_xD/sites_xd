<?php
include("parsedown/Parsedown.php");

function GetSiteDir($site_name) {
    return "/var/www/html/private/sites/".$site_name."/".$site_name;
}
function ReadSite($site_name, $edit="false") {
    // Does the site we're trying to access exist? If not, output a 404.
    if(!file_exists(GetSiteDir($site_name).".sxd")) {
        return "404";
    // Otherwise continue with the function.
    } else {
        // Open the file and prepared for reading.
        $site = fopen(GetSiteDir($site_name).".sxd", "r");
        // If there was a site found...
        if ($site) {
            // By default boxes are not nested.
            $nested = false;
            $wasnested = false;
            // This variable is for generating ids in edit mode.
            $i = 1;
            // Initialize functions used from other packages
            $Parsedown = new Parsedown();
            $Parsedown->setBreaksEnabled(true);
            // For every line of code in said site...
            while (($line = fgets($site)) !== false) {
                // Is this a nested box? If so then set $nested.
                if (strpos($line, '}[') !== false) {$nested = true;}
                // But we don't set $nested to false until the nest is terminated 
                // (hence why $nested was set to false earlier)
                if (strpos($line, '}]') !== false) {$nested = false; $wasnested = true;}
                // Firstly get an unmodified copy of the line.
                // This was actually added for something completely different
                // but it makes the markdown happy so I guess it stays.
                $line_un = $line;
                // But we do specifically want the last character gone,
                // because that's always "}".
                $line_un = substr($line_un,0,-1);
                // Then, modify the current line, starting by stripping the curly braces, and brackets...
                $line = str_replace("{","",$line); $line = str_replace("}","",$line); $line = str_replace("[","",$line); $line = str_replace("]","",$line);
                // Split the attributes of the line into variables.
                $attributes = explode("|", $line);
                // Get the attributes but with the unmodified version.
                $attributes_un = explode("|", $line_un);
                // Create the first part of the div.
                echo("<div class='box' ");
                if($edit == "true") {echo "id='b".$i."' oncontextmenu='return false;'";}
                echo(" style='display: block;padding:5px;position:absolute;overflow:hidden;");
                // For each attribute we've found...
                // starting with the unmodified version...
                foreach ($attributes_un as $attribute_un) {
                    // Split each attribute into their arguments.
                    $arg = explode("=", $attribute_un);
                    // yeah this foreach was taked on at the last second
                    // we just want the content. 
                    if($arg[0] == "content") {$fuckyou = custom_htmlspecialchars($arg[1]); $fuckme = $arg[1];}
                }
                // Now onto the ones we do care about.
                foreach ($attributes as $attribute) {
                    // First we split each attribute again into their arguments.
                    $arg = explode("=", $attribute);
                    // This is very self explanatory so not every one will be commented.
                    // Essentially we choose the css style to place based on the first argument,
                    // and the value for that style is based on the second one.
                    $saysthetrollface = custom_htmlspecialchars($arg[1]);
                    switch($arg[0]) {
                        case "content":
                            // actually the content variable is important,
                            // because if we're in edit mode the content should
                            // not be in markdown and should be put in a textarea.
                            if($edit == "true") {
                                $content = "<textarea class='content'>".str_replace('<br>','&#13;&#10;',$fuckme)."</textarea><span class='handle'></span>";
                            } else {
                                $content = $Parsedown->line(str_replace("<br>","\n",$fuckyou));
                            }
                            break;
                        case "x":
                            echo("left: ".$saysthetrollface."%;");
                            break;
                        case "y":
                            echo("top: ".$saysthetrollface."%;");
                            break;
                        case "width":
                            echo("width: ".$saysthetrollface."%;");
                            break;
                        case "height":
                            echo("height: ".$saysthetrollface."%;");
                            break;
                        case "bgcolor":
                            echo("background-color: ".$saysthetrollface.";");
                            break;
                        case "bgimage":
                            echo("background-image: url(".$saysthetrollface.");");
                            break;
                        case "txcolor":
                            echo("color: ".$saysthetrollface.";");
                            break;
                        case "txsize":
                            echo("font-size: ".$saysthetrollface.";");
                            break;
                        case "curve":
                            echo("border-radius: ".$saysthetrollface);
                            break;
                        case "anchor":
                            echo("transform: translate(");
                            switch($saysthetrollface) {
                                case "topcenter":
                                    echo "-50%, 0);";
                                    break;
                                case "topright":
                                    echo "-100%, 0);";
                                    break;
                                case "centerleft":
                                    echo "0, -50%);";
                                    break;
                                case "center":
                                    echo "-50%, -50%);";
                                    break;
                                case "centerright":
                                    echo "-100%, -50%);";
                                    break;
                                case "bottomleft":
                                    echo "0, -100%);";
                                    break;
                                case "bottomcenter":
                                    echo "-50%, -100%";
                                    break;
                                case "bottomright":
                                    echo "-100%, -100%";
                                    break;
                                default:
                                    echo "0, 0);";
                                    break;
                            }
                            break;
                        case "shadow":
                            echo("box-shadow: ");
                            switch($saysthetrollface) {
                                case "light":
                                    echo "5px 5px 10px 0px rgba(0,0,0,0.75);";
                                    break;
                                case "medium":
                                    echo "10px 10px 15px 0px black;";
                                    break;
                                case "harsh":
                                    echo "5px 5px 0px 0px black;";
                                    break;
                                default:
                                    echo $saysthetrollface;
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                }
                // Now is a good time to check the content,
                // because if it's only "LOGIN", AND the site happens
                // to be the homepage, then the content needs to be
                // customized.
                if($content == "LOGIN" && $site_name == "home") {
                    echo("'>");include("login.php");echo("</div>");
                // Otherwise...
                } else {
                    // End the div and add the content we got earlier.
                    if($nested == false) {
                        echo("'>$content</div>");
                    // But if this is a nested box then don't add "</div>" until the nest is termintaed.
                    } elseif ($nested == true) {
                        echo("'>$content"); 
                    }
                    if ($wasnested == true) {
                        echo("</div>"); 
                        $wasnested == false;
                    }
                }
                $i++;
            }
            // Close the file.
            fclose($site);
        }
    }
}
?>