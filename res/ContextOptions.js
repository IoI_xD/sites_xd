  var correlations = [{'name': 'bgtab', 'goto': 'bgOptions'}, {'name': 'txtab', 'goto': 'txOptions'}, {'name': 'sitetab', 'goto': 'siOptions'}, {'name': 'misctab', 'goto': 'miOptions'}, {'name': 'botab', 'goto': 'boOptions'}, {'name': 'hidden', 'goto': 'nope'}];
  var menuOptions = [{'name': 'Background', 'id': 'bgtab'}, {'name': 'Text', 'id': 'txtab'}, {'name': 'Border', 'id': 'botab'}, {'name': 'Misc.', 'id': 'misctab'}, {'name': 'Site Settings', 'id': 'sitetab'}, {'name': 'Hidden.', 'id':'hidden'}];
  var yeOptions = [{'name': 't', 'id':'tab'}];
  var bgOptions = [{'name': 'Color', 'id': 'bgcolor'}, {'name': 'Image', 'id': 'bgimage'}];
  var txOptions = [{'name': 'Color', 'id': 'txcolor'}, {'name': 'Size',  'id': 'txsize'}];
  var boOptions = [{'name': 'Curve', 'id': 'bocurve'}, {'name': 'Image', 'id': 'boimage'}];
  var miOptions = [{'name': 'Anchor', 'id': 'anchor'}, {'name': 'Delete Box', 'id': 'del'}];
  var siOptions = [{'name': 'Save Site', 'id': 'savesite'}, {'name': 'Change Site Background', 'id':'sitebg'}]
