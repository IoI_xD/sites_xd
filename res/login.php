<?php
session_start();
if($_POST) {
	$password = sites_get($_POST['username'], 'password');
	if(!$password) {
		echo("Account does not exist.");
	} else {
		if(password_verify($_POST['password'], $password)) {
			$_SESSION['valid'] = true;
			$_SESSION['username'] = $_POST['username'];
			header("Location: /");
		} else {
			print("password incorrect.");
		}
	}
}
if(isset($_SESSION['valid']) && $_SESSION['valid'] == true) {
$username = $_SESSION['username'];
?>
<center>
	<b>Welcome, <?php echo $username;?>!</b><br><br>
	<em>You have <?php if (msg_get($username) && count(msg_get($username)) > 0) {echo msg_get($username)." unread";} else {echo "no";} ?> messages.</em>
</center>
<?php
} else {
?>
<center>
<b>Login</b><br><br>
<form method="POST">
	<input type="text" name="username" placeholder="Username"><br><br>
	<input type="password" name="password" placeholder="Password"><br><br>
	<input type="submit" name="submit" value="Login">
</form></center>
<?php
}
?>