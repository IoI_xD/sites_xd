// some code that i stole from stack overflow
function rgb2hex(rgb) {if(/^#/.test(rgb))return rgb;let re = /\d+/g;let hex = x => (x >> 4).toString(16)+(x & 0xf).toString(16);return "#"+hex(re.exec(rgb))+hex(re.exec(rgb))+hex(re.exec(rgb));}
function hex2rgb(hex) {var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);return result ? {r: parseInt(result[1], 16),g: parseInt(result[2], 16),b: parseInt(result[3], 16)} : null;}

// this one i wrote on my own that's probably why it probably uscks
function rgb2obj(rgb) {return {r: parseInt(rgb[1], 16),g: parseInt(rgb[2], 16),b: parseInt(rgb[3], 16)}}

//
// general shit
//

function settingsShow(html, showClose="yes") {
	// if there's a dropdown menu remove it
	contextClear(0);
	// add the class to the settings menu that shows it
	$("#settings").addClass("show");
	// in addition to any html that we said to be added here we add a close button, unless said otherwise.
	if(showClose != "no") {
		$("#settings").html(html+"<br><input type='button' value='close' onclick='settingsHide();'>");
	} else {
		$("#settings").html(html);
	}
	return;
}
function settingsHide() {
	// remove the class on the settings menu that shows it.
	$("#settings").removeClass("show");
	// clear the html.
	$("#settings").html("");
}

//
// background shit
//

function bgcolor() {
	// get the background color
	var bgcolor= {hex: rgb2hex($("#"+selBox).css('background-color')), rgb: rgb2obj($("#"+selBox).css('background-color')), raw: $("#"+selBox).css('background-color')};
	// get the opacity.
	var opacity=bgcolor.rgb.a;
	// pull up the settings menu.
	settingsShow(`<input id='color' type='color' value='`+bgcolor.hex+`'><br>
		<input id='opacity' type='range' min='0' max='100' value='`+opacity*100+`'>`);
	// when the color is changed, or when the slider is moved (note: .change() is not used because this method always gives us a live output, while .change() does not do this on firefox)
	$('#color, #opacity').on('input', function (event) {
		// get the color from the picker in rgb
		bgcolor = hex2rgb($("#color").val());
		// get the value of the slider
		opacity=$("#opacity").val();
		// update the box's colors accordingly.
		$("#"+selBox).css({"background-color": "rgba("+bgcolor.r+","+bgcolor.g+","+bgcolor.b+","+opacity/100+")"});
	});
}

function bgimage() {
	var sizeval = [{'value': 'auto', 'name': 'Automatic'}, {'value': '100% 100%', 'name': 'Stretched'}, {'value': 'contain', 'name': 'Contained'}, {'value': 'cover', 'name': 'Cover'}, {'value':'custom', 'name': 'Specified (advanced)'}];
	var repeatval = [{'value': 'no-repeat', 'name': 'Don\'t repeat.'}, {'value': 'repeat-x', 'name': 'Repeat along the x axis'}, {'value': 'repeat-y', 'name': 'Repeat along the y axis'}, {'value': 'repeat', 'name': 'Repeat along both axises'}];
	var bgimage = $("#"+selBox).css("background-image").replace("url(","").replace(")","").replace("\"",""); 
	if (bgimage == "none") {bgimage = ""};
	var bgsize = $("#"+selBox).css("background-size") || "auto"; if(bgsize.includes('px') || (bgsize.includes('%') && bgsize != "100% 100%")) {var bgsize_v = "custom"; var show = 'inline-block';} else {var bgsize_v = bgsize; var show='none'};
	var bgrepeat = $("#"+selBox).css("background-repeat") || "repeat";
	result = `<input type='text' placeholder='The url for the image goes here.' size='50' value='${bgimage}' id='image'><br><br>
		<b>Background scaling:</b> <select id='size' style='display: inline-block;' name='size'>`;
	$.each(sizeval, function(key, val) {
		result += `<option value=${val.value}`;
		if(val.value == bgsize_v) {result += ` selected='selected'`;}
		result += ` >${val.name}</option>`;
	})
	result += `</select><br>
	<b>Background repeating: </b><select id='repeat' name='repeat'>`;
	$.each(repeatval, function(key, val) {
		result += `<option value=${val.value}`;
		if(val.value == $("#"+selBox).css('background-repeat')) {result += ` selected='selected'`;}
		result += ` >${val.name}</option>`;
	})
	result += `<input style='display: ${show};' id='custom' type='text' placeholder='X[px/%] Y[px/%]' value='${bgsize}'><br>
		<a target='_blank' href='https://sxd.ioi-xd.net/i'>Check out the image center to see images others have uploaded!</a>`;
	settingsShow(result);
	// when the input boxes are updated, update the selected box with the value.
	$('#image, #size, #repeat, #custom').on('input', function(event) {
		if ($('#size').val() == "custom"){
		  $('#custom').css({"display": "inline-block"});
		  bgsize = $('#custom').val();
		} else {
			$('#custom').css({"display": "none"});
			bgsize = $('#size').val();
		}
		console.log(bgsize);
		$("#"+selBox).css({"background-image": "url("+$('#image').val()+")", "background-size": bgsize, "background-repeat": $("#repeat").val()});
	});

}

//
// text shit
//

function txcolor() {
	// get the text color.
	var txcolor = $("#"+selBox).css('color');
	// show the relevant settings box.
	settingsShow("<input id='color' type='color' value='"+txcolor+"'>")
	// when the color chooser is updated...
	$('#color').on('input', function (event) {
		// get the color from the picker in rgb
		txcolor = rgb2hex($("#color").val());
		// update the box's colors accordingly.
		$("#"+selBox).css({"color": txcolor});
	});
}

function txsize() {
	// get the text size
	var txsize = $("#"+selBox).css('font-size');
	// show the settings box
	settingsShow(`<input id='size_m' type='input' size='1' value='`+txsize.replace('px','')+`'>
		<select name='types' id='types'>
			<option value='px'>pixels</option>
			<option value='%'>% of the screen</option>
			<option value='em'>em</option>
			<option value='rem'>rem</option>
			<option value='ex'>ex</option>
		</select>&nbsp&nbsp
		<input id='size_f' type='range' min='0' max='100' '`+txsize.replace('px','')+`'>`);
	// when the slider is updated
	$('#size_f').on('input', function(event) {
		// get the value 
		txsize = $("#size_f").val();
		// update the input box
		$('#size_m').val(txsize);
		// update the selected box
		$("#"+selBox).css({"font-size": txsize+""+$("#types").val()});
	})
	// when the input box is updated
	$('#size_m, #types').on('input', function(event) {
		// get the value 
		txsize = $("#size_m").val();
		// if the value is over 100 then disable the slider
		if (txsize > 100) {
			$("#size_f").prop('disabled',true);
		} else {
			$("#size_f").prop('disabled',false);
		}
		// update the slider box
		$('#size_f').val(txsize);
		// update the selected box
		$("#"+selBox).css({"font-size": txsize+""+$("#types").val()});
	})
}

//
// border shit
//

function bocurve() {
	// get the current curve of the border.
	var bocurve = $("#"+selBox).css('border-radius').replace('.001','') || "0% 0% 0% 0%";
	var curves = bocurve.split(" ");
	console.log(curves);
	settingsShow(`
		<table>
			<tr>
				<td><input id='size_1' style='width: 50px' type='range' min='0' max='100' value='${curves[0].replace('%','')}'></td>
				<td><input id='size_2' style='width: 50px' type='range' min='0' max='100' value='${curves[1].replace('%','')}'></td>
			</tr>
			<tr>
				<td><input id='size_3' style='width: 50px' type='range' min='0' max='100' value='${curves[2].replace('%','')}'></td>
				<td><input id='size_4' style='width: 50px' type='range' min='0' max='100' value='${curves[3].replace('%','')}'></td>
			</tr>
				`);
	// when the slider is updated
	$('#size_1, #size_2, #size_3, #size_4').on('input', function(event) {
		// update the selected box
		$("#"+selBox).css({"border-radius": $("#size_1").val()+".001% "+$("#size_2").val()+".001% "+$("#size_4").val()+".001% "+$("#size_3").val()+".001% "});
	})
}

function boimage() {
	settingsShow(`<h1>This is a feature that will be added soon, but for now it takes up space in the context menu.</h1>`)
}

//
// misc. shit
//

function anchor() {
	settingsShow(`<h1>This is a feature that will be added eventually, but for now it takes up space in the context menu.</h1>`)
}

function del() {
	settingsShow("<b>Are you sure you want to delete the box? Once you save the website this cannot be undone.</b><br><input type='button' id='yes' value='Yes'> <input id='no' type='button' value='No'>", "no");
	$('#yes').click(function() {
		$("#"+selBox).remove();
		settingsHide();
	})
	$('#no').click(function() {
		settingsHide();
	})
}
function hidden() {return;}

//
// site settings
//

function topercentage_width(num) {
	return ( 100 * parseFloat(num) / parseFloat($(document).width()) );
}
function topercentage_height(num) {
	return ( 100 * parseFloat(num) / parseFloat($(document).height()) );
}
function savesite() {
	var boxes = [];
	// if there's a dropdown menu remove it
	contextClear(0);
	$('#canvas').children().each(function (index) {
		// ok so basically
		var boxtx = `{
x=${topercentage_width($(this).css('left'))}|
y=${topercentage_height($(this).css('top'))}|
width=${topercentage_width($(this).css('width'))}|
height=${topercentage_height($(this).css('height'))}|
bgcolor=${$(this).css('background-color')}|
bgimage=${$(this).css('background-image').replace('src(\"','').replace('\")')}|
txcolor=${$(this).css('color')}|
txsize=${$(this).css('font-size')}|
curve=${$(this).css('border-radius') || "0"}|
content=${$(this).find('.content').val().replace(/\n/g,'<br>')}
}`;
		boxes.push(boxtx.replace(/\n/g,''))
	});
	var finalresult = "";
	$.each(boxes, function(key, val) {
		finalresult = finalresult+"\n"+val;
	})
	var password = prompt("Enter your account password.");
	$.post( "https://sxd.ioi-xd.net/res/upload.php", { contents: ""+finalresult+"", password: password }).done(function( data ) {
    	alert(data);
  	});
}

//{x=50|y=50|width=30|height=50|bgcolor=#004400|bgimage=https://sxd-img.ioi-xd.net/rocktile/rocktile.png|txcolor=#fff|txsize=13|anchor=center|shadow=light|curve=5|content=i shit my pants haha wee **yayayhoo** (<b>html injection isn't allowed lol</b>)}