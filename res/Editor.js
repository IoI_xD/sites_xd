$(document).ready(function(){
  // Initialize variables.
  var docWidth = $(document).width(); var docHeight = $(document).height();
  // On mouse any mouse movement...
  $("#canvas").mousemove(function(event) {
    // Save the current x and y position of the mouse.
    mouse.x = event.pageX;
    mouse.y = event.pageY;
    // If the test mode is enabled, the mouse coords will be printed in the top corner of the screen
    $("#mouse_coords").text(mouse.x+", "+mouse.y);
    // This is also where we control the dotted guides.
    if(mousedown == 1 && !transformMode) {
      // Get the width/height of what the dotted guide should be.
      var width = (Math.abs(x1-mouse.x)).toString(); var height = (Math.abs(y1-mouse.y)).toString();
      if(x1 <= mouse.x) {ax = x1} else {ax = mouse.x}
      if(y1 <= mouse.y) {ay = y1} else {ay = mouse.y}
      $("#guide1").css({'border':'1px dashed black', 'top':ay.toString(),'left':ax.toString(),'width':width,'height':height});
    }
    if (boxTriggersX.includes(Math.floor(mouse.x))) {
      $("#guide2").css({height:'100%', left: mouse.x, top: 0, border: '1px dashed rgba(255,0,0,0.3)'});
    } else {$("#guide2").css({height:'0px'})}

    if (boxTriggersY.includes(Math.floor(mouse.y))) {
      $("#guide2").css({width:'100%', top: mouse.y, left: 0, border: '1px dashed rgba(255,0,0,0.3)'});
    } else {$("#guide2").css({width:'0px'})}


    xp = (mouse.x/docWidth)*100; yp = (mouse.y/docHeight)*100;
    if(xp%10 < .5) {
      $("#guide4").css({height:'100%', left: xp+"%", border: '1px dashed rgba(0,0,255,0.3)'})
    } else {
      $("#guide4").css({height:'0px'})
    }
    if(yp%10 < .5) {
      $("#guide5").css({width:'100%', top: yp+"%", border: '1px dashed rgba(0,0,255,0.3)'})
    } else {
      $("#guide5").css({width:'0px'})
    }
    // get the last box we hovered over
    box = $('.box:hover, .contextOption:hover').last();
  });
  // When the mouse buttom is held down.
  $("#canvas").mousedown(function(event) {
    // if we're actually selecting a box nothing the textarea should override the effect.
    if(box.length != 0) {
      console.log("");
    } else {
      // get the x/y coords
      x1 = mouse.x; y1 = mouse.y;
      switch(event.which) {
      // if there was a left click
      case 1:
        mousedown = 1;
        break;
      // if there was a middle click
      case 2:
        // toggle transform mode
        transformMode = !transformMode;
        // and add a class to the body tag regarding this
        $("#doc").toggleClass("transform");
        // make all boxes draggable and resizable
        $(".box").draggable({handle: ".handle"});
        $(".box").resizable();
        break;
      // if there was a right click
      case 3:
        // fuck right off
        return;
        break;
      // if we're here then the user has some stupid fucking gamer mouse so feel free to skip this.
      default:
        break;
      }
    }
    // if the music hasn't already started playing
    if(userActivated == 0) {
      // play it, bitch.
      var audio = new Audio('res/saysthetrollface.mp3');
      audio.play();
      // but don't play it more then once.
      userActivated = 1;
    }
  });
  // when the mouse button is released.
  $("#canvas").mouseup(function(event) {
    // regardless of which mouse button was released now is a good time
    // to hide the settings box
    settingsHide();
    // if we right clicked...
    if(event.which == 3) {
      // create the context menu
      contextCreate($("#doc"), menuOptions, mouse.x, mouse.y);
      return;
    } else {
    // if we're actually selecting a box nothing the textarea should override the effect.
    if(box.length != 0) {
      console.log("");
    } else {
      // well the mouse button is released
      mousedown = 0;
      // but if the left button was released and we're not in transform mode we need to do more, because a box was just drawn.
      if (event.which == 1 && !transformMode) {
      // get rid of the first guide.
      $("#guide1").css({'top':'0','left':'0','width':'0px','height':'0px','pointer-events': 'none'});
      // bind the current mouse coords to another variable.
      x2 = mouse.x; y2 = mouse.y;
      // get the width/height of the box that was just drawn.
      width = ((Math.abs(x1-x2)/docWidth)*101); height=((Math.abs(y1-y2)/docHeight)*102);
      // if the box is 2px in either direction then it's not counted.
      if(((width <= 2) && (width >= -2)) || ((height <= 2) && (height >= -2))) {return};
      // the anchor x/y is set to whichever x/y is lowest
      if(x1 <= x2) {ax = x1} else {ax = x2}
      if(y1 <= y2) {ay = y1} else {ay = y2}
      // push the coords to their respective arrays
      boxTriggersX.push(x1,x2);
      boxTriggersY.push(y1,(Math.round(ay+Math.abs(y1-y2))));
      // oh yeah boxes have IDs, but the number can just be the amount of boxes created thus far. 
      boxId++;
      // append our calculated box to the canvas.
      $("#canvas").append("<div class='box' id='b"+boxId+"' oncontextmenu='return false;' style='width: "+width+"%; height: "+height+"%; left: "+ax+"px; top: "+ay+"px; background-color: rgb(128,128,255, 0.5); color: #000000; font-size: 14px;'><textarea class='content'></textarea><span class='handle'></span></div>");
      }
    }
  }
  });
});