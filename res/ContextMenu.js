  // The function for creating context menus
  function contextCreate(parent, arr, x, y) {
    // Get the id of either the box that the context menu needs to modify,
    // or the context option that's being clicked on. 
    // (event.target is for turbovirgins)
    var selected = box.attr('id');
    if (/b[0-9]/.test(selected)) {selBox = selected};
    // If there's no box there's no context menu.
    if (selected == undefined) {return -1;}
    // If we're clicking a box then an entirely new context menu is created
    // and all others should be gone.
    if (box.attr('class') == "box") {contextClear(0);}
    // Append a context menu to whatever parent was specified.
    parent.prepend("<span class='context' oncontextmenu='return false;' id='"+connest+"' style='z-index:"+(999+connest)+"; left: "+x+"px; top: "+y+"px;'></span>");
    // Clear the context menu with the id if it exists currently.
    $(".context#"+connest).html("");
    // Clear whenclick
    whenclick = "";
    // For each value in the array we were given...
    $.each(arr, function(key, val) {
      // Get the context menu that corresponds to the value.
      $.each(correlations, function(key2, val2) {
        // i can't be assed to comment this because this actually
        // makes no sense and for some reason this is valid in jquery??
        if (val.id == val2.name) {corrArr = val2.goto;}
        if(corrArr == "nope") {
          whenclick = "onclick='"+val.id+"();'";
          arrow = "";
        } else {
          arrow = "<span class='arrow' onclick=\"contextClear(1); box = $('.box:hover, .contextOption:hover').last(); contextCreate(box, "+corrArr+", box.width(), 'NULL'); return false;\"></span>";
        }
      });
      // Add something to the context menu.
      $(".context#"+connest).append("<span class='contextOption' "+whenclick+" id='"+val.id+"'>"+val.name+""+arrow+"</span>");
    })
    // Append the nest number by one.
    connest += 1;
  }
    // Quick and dirty function for clearing any context menus after the first one.
    function contextClear(after) {
      for (var n = after; n < 5; n++) {
        $(".context#"+n).remove();
      }
      connest = after;
    }