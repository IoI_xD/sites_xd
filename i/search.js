$(document).ready(function(){
	function url_content(url){
		return $.ajax({type: "GET", url: "https://sxd.ioi-xd.net/i/search.php?q="+url, async: true});
	}
	function toclipboard(text) {
		$('.results').append("<textarea col=0 rows=0 class='clip'>"+text+"</textarea>");
		$('.clip').select();
		try {
			console.log(document.execCommand("copy"));
		}
		catch (ex) {
			console.warn(ex);
			return false;
		}
		finally {
			$('.clip').remove();
		}
	}
	$('.search').on('input', function (event) {
		url_content($('.search').val()).then(function successCallback(response) { 
  			$('.results').html(response);
  			$('img').mousedown(function (event) {
  				if(event.which == 2) {
  					$(this).addClass('copied');
  				}
  			});
  			$('img').mouseup(function (event){	
				if(event.which == 2) {
					toclipboard($(this).attr('src'));
					$(this).removeClass('copied');
				}
			});
		});
	});
});
