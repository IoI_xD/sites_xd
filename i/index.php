<?php
$error = ""; $success = "";
// Display all errors.
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include "bulletproof.php";

if(!empty($_POST)) {
  $error = "";
  if(empty($_POST['name'])) {
    $error .= "name cannot be empty. ";
  }
  if (empty($_POST['desc'])) {
    $error .= "description cannot be empty. ";
  }
  if(preg_match("/^([^A-z][^0-9]-_)$/", $_POST['name'])) {
      $error .= "image name can only contain the standard American alphabet and/or letters 0 through 9. no spaces allowed.<br>
      <small>for nerds, the regex is /^([^A-z][^0-9]-_)$/</small>";
  }
  foreach (scandir('../../private/images/') as &$i) {
      if($i == $_POST['name']) {
        $error .= "a file with this exact name exists.";
        break;
      }
  }
  if(empty($error)) {
    $image = new Bulletproof\Image($_FILES);

    $image->setStorage("../../private/images/".htmlspecialchars($_POST['name']), 0755);
    $image->setSize(0, 1500000);
    if($image["pictures"]){
      if($image->upload()){
        $success = "uploaded!";
        $desc = fopen("../../private/images/".htmlspecialchars($_POST['name'])."/desc.txt", "w") or die("Unable to open file!");
        fwrite($desc, $_POST['desc']);
        fclose($desc);
      } else {
        $success = $image->getError();
      }
    } else {
      $error .= "there's no image";
    }
  }
}
?>
<html>
  <head>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DotGothic16&family=Ubuntu&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="css.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="search.js"></script>
    <title>image center</title>
  </head>
  <body>
    <center>
      <h1>Image Center</h1>
      <p>Images you upload to hindsite are publically avaliable from an image chooser. To avoid too many similar images being uploaded, we ask that you first look through the database to see if there are similar images you can use. <em>We also ask that you don't upload gore, hateful imagery, or illegal content.</em> Copyrighted material is allowed but subject to DMCA and we ask that you try to upload original content when you can.</p>
    </center>
    <span class="table">
      <span class="thead">
      <span class="tr">
        <span class="td"><h3>Search</h3></span>
      </span><span class="tr">
        <span class="td"><h3>Upload</h3></span>
      </span>
    </span><span class="tbody">
      <span class="tr"><span class="td">
        <input type="text" size='100' class='search' placeholder="Type 'all' to see every image currently uploaded."><br><span class='results'><em>(type in the above bar to search for an image, middle click any of the images to copy their link)</em></span>

      </span></span><span class="tr"><span class="td">
    <em>Please be descriptive in the fields and <strong>refrain from putting in random bullshit</strong></em><br>

    <form name="form" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="MAX_FILE_SIZE" value="2000000"/>
        <b>Name: </b><input type="text" name="name" size="30"/><br><br>
      
      <b>Description: </b><input type="text" name="desc" size="30"/><br>
      <input type="file" name="pictures" accept="image/*"/><input type="submit" value="upload"/>
    </form><br>
    <?php echo $success." ".$error;?>
  </span class="td"></span class="tr">
</tbody>
</table>
  </body>
</html>