<?php
$q = htmlspecialchars($_GET['q']);
$dir    = '../../private/images/';
$files = scandir($dir);
// for each folder in the directory we scanned
foreach ($files as &$i) {
	// is the query we've specified in the foldername?
	// if not, , and the query isn't "all", do nothing.
	if(strpos($i, $q) === false && $q != "all") {
		echo "";
	} else {
		// if so, scan the directory in question. 
		$yeah = scandir($dir."/".$i);
		// for each file in the directory...
		foreach ($yeah as &$j) {
			// if it's not a .png, .gif, or .jpg, don't display it.
			if(strpos($j, ".png") === false && strpos($j, ".gif") === false && strpos($j, ".jpg") === false) {
				echo "";
			} else {
			// otherwise go ahead.
				$desc = file_get_contents("../../private/images/".$i."/desc.txt");
				echo "<span class='image'><span class='info'><span class='name'>".htmlspecialchars($i)."</span><span class='desc'>".htmlspecialchars($desc)."</span></span><img src='https://sxd-img.ioi-xd.net/".$i."/".$j."'></span>";
			}
		}
	}
}
?>
